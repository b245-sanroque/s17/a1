console.log("Hello World");

/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:	
	let myName = prompt("What is your name?");
	let myAge = prompt("How old are you?");
	let myAddress = prompt("Where do you live?");

	console.log("Hello, "+myName+".");
	console.log("You are "+myAge+" years old.");
	console.log("You live in "+myAddress+".");

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:	
	function getBands(){
		let musicBands = ["Twice","GFriend","Mamamoo","flowers (E-girls)","SB19"];
		console.log("1. "+musicBands[0]);
		console.log("2. "+musicBands[1]);
		console.log("3. "+musicBands[2]);
		console.log("4. "+musicBands[3]);
		console.log("5. "+musicBands[4]);
	}
	getBands();

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:
	function getMovies(){
		let movies = ["The Godfather","The Godfather, Part II","Shawshank Redemption","To Kill a Mockingbird","Psycho"];
		let ratings = ["97","96","91","93","96"];
		console.log("1. "+movies[0]);
		console.log("Rotten Tomato Rating: "+ratings[0]+"%");
		console.log("1. "+movies[1]);
		console.log("Rotten Tomato Rating: "+ratings[1]+"%");
		console.log("1. "+movies[2]);
		console.log("Rotten Tomato Rating: "+ratings[2]+"%");
		console.log("1. "+movies[3]);
		console.log("Rotten Tomato Rating: "+ratings[3]+"%");
		console.log("1. "+movies[4]);
		console.log("Rotten Tomato Rating: "+ratings[4]+"%");	
	}
	getMovies();
/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/

alert("Hi! Please add the names of your friends.");
let friend1 = prompt("Enter your first friend's name:"); 
let friend2 = prompt("Enter your second friend's name:"); 
let friend3 = prompt("Enter your third friend's name:");

let printFriends = function printUsers(){
	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3);
printUsers();
};

console.log(friend1);
console.log(friend2);
console.log(friend3);